import 'package:app_despesas/widgets/grafico.dart';
import 'package:app_despesas/widgets/nova_transacao.dart';
import 'package:app_despesas/widgets/transacao_list.dart';
import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'models/transacao.dart';

void main() {
  // WidgetsFlutterBinding.ensureInitialized();
  // SystemChrome.setPreferredOrientations(
  //   [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown],
  // );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    initializeDateFormatting('pt_BR', null);

    return MaterialApp(
      title: 'Despesas Pessoais',
      home: MyHomePage(),
      theme: ThemeData(
          primarySwatch: Colors.purple,
          accentColor: Colors.amber,
          errorColor: Colors.red,
          fontFamily: 'Quicksand',
          textTheme: ThemeData.light().textTheme.copyWith(
                headline6: TextStyle(
                    fontFamily: 'OpenSans',
                    fontWeight: FontWeight.bold,
                    fontSize: 18),
                button:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
          appBarTheme: AppBarTheme(
              textTheme: ThemeData.light().textTheme.copyWith(
                  headline6: TextStyle(
                      fontFamily: 'OpenSans',
                      fontSize: 20,
                      fontWeight: FontWeight.bold)))),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final List<Transacao> _transacoes = [
    //Transacao(id: 't1', titulo: 'Sapato', valor: 200.00, data: DateTime.now()),
    //Transacao(id: 't2', titulo: 'Camisa', valor: 100.90, data: DateTime.now()),
    //Transacao(id: 't3', titulo: 'Mercado', valor: 500.00, data: DateTime.now())
  ];

  bool _mostrarGrafico = true;

  List<Transacao> get _transacoesRecentes {
    return _transacoes.where((element) {
      return element.data.isAfter(DateTime.now().subtract(
        Duration(days: 7),
      ));
    }).toList();
  }

  void _adicionarNovaTransacao(String titulo, double valor, DateTime data) {
    final nova = Transacao(
        titulo: titulo,
        valor: valor,
        data: data,
        id: DateTime.now().toString());

    setState(() {
      _transacoes.add(nova);
    });
  }

  void _removerTransacao(String id) {
    setState(() {
      _transacoes.removeWhere((element) => element.id == id);
    });
  }

  void _mostrarNovaTransacao(BuildContext ctx) {
    showModalBottomSheet(
        context: ctx,
        builder: (_) {
          return GestureDetector(
            child: NovaTransacao(_adicionarNovaTransacao),
            onTap: () {},
            behavior: HitTestBehavior.opaque,
          );
        });
  }

  List<Widget> _buildPanorama(tamanhoTotal) {
    List<Widget> widgets = [];
    widgets.add(Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('Mostrar Gráfico'),
        Switch.adaptive(
            value: _mostrarGrafico,
            onChanged: (val) {
              setState(() {
                _mostrarGrafico = val;
              });
            })
      ],
    ));

    if (_mostrarGrafico) {
      widgets.add(
        Container(
          height: tamanhoTotal * 0.67,
          child: Grafico(_transacoesRecentes),
        ),
      );
    }
    widgets.add(Container(
      height: tamanhoTotal * 0.67,
      child: TransacaoList(_transacoes, _removerTransacao),
    ));
    return widgets;
  }

  List<Widget> _buildRetrato(tamanhoTotal) {
    Widget grafico = Container(
        height: tamanhoTotal * 0.33, child: Grafico(_transacoesRecentes));

    Widget transacoesWidget = Container(
        height: tamanhoTotal * 0.67,
        child: TransacaoList(_transacoes, _removerTransacao));

    return [grafico, transacoesWidget];
  }

  @override
  Widget build(BuildContext context) {
    bool isPanorama =
        MediaQuery.of(context).orientation == Orientation.landscape;

    final appBar = AppBar(
      title: const Text('Despesas Pessoais'),
      actions: <Widget>[
        IconButton(
            icon: Icon(Icons.add),
            onPressed: () => _mostrarNovaTransacao(context))
      ],
    );
    final tamanhoTotal = MediaQuery.of(context).size.height -
        appBar.preferredSize.height -
        MediaQuery.of(context).padding.top;
    return Scaffold(
      appBar: appBar,
      body: SingleChildScrollView(
        child: Column(
          children: [
            if (isPanorama) ..._buildPanorama(tamanhoTotal),
            if (!isPanorama) ..._buildRetrato(tamanhoTotal),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () => _mostrarNovaTransacao(context)),
    );
  }
}

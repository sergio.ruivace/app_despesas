import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class NovaTransacao extends StatefulWidget {
  final Function novaTransacao;

  NovaTransacao(this.novaTransacao);

  @override
  _NovaTransacaoState createState() => _NovaTransacaoState();
}

class _NovaTransacaoState extends State<NovaTransacao> {
  final _tituloController = TextEditingController();
  final _valorController = TextEditingController();
  DateTime _dataSelecionada;

  void enviarDados() {
    if (_tituloController.text.isEmpty || _valorController.text.isEmpty) {
      return;
    }

    final tituloInput = _tituloController.text;
    final valorInput = double.parse(_valorController.text);

    if (tituloInput == null || valorInput <= 0 || _dataSelecionada == null) {
      return;
    }
    widget.novaTransacao(tituloInput, valorInput, _dataSelecionada);
    Navigator.of(context).pop();
  }

  void _mostrarDatePicker() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2019),
      lastDate: DateTime.now(),
    ).then((value) {
      if (value == null) {
        return;
      } else {
        setState(() {
          _dataSelecionada = value;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Card(
        elevation: 5,
        child: Container(
          padding: EdgeInsets.only(
            top: 10,
            left: 10,
            right: 10,
            bottom: MediaQuery.of(context).viewInsets.bottom + 10,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              TextField(
                decoration: InputDecoration(labelText: 'Título'),
                controller: _tituloController,
                onSubmitted: (_) => enviarDados(),
              ),
              TextField(
                decoration: InputDecoration(labelText: 'Valor'),
                controller: _valorController,
                keyboardType: TextInputType.number,
                onSubmitted: (_) => enviarDados(),
              ),
              Container(
                height: 70,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(_dataSelecionada == null
                          ? 'Data não escolhida'
                          : 'Data: ${DateFormat('dd/MM/yyyy').format(_dataSelecionada)}'),
                    ),
                    FlatButton(
                      textColor: Theme.of(context).primaryColor,
                      child: Text('Escolha a data',
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      onPressed: _mostrarDatePicker,
                    )
                  ],
                ),
              ),
              RaisedButton(
                child: Text('Adcionar Despesa'),
                color: Theme.of(context).primaryColor,
                textColor: Theme.of(context).textTheme.button.color,
                onPressed: enviarDados,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:app_despesas/models/transacao.dart';
import 'package:app_despesas/widgets/barra_grafico.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Grafico extends StatelessWidget {
  final List<Transacao> transacoesRecentes;

  Grafico(this.transacoesRecentes);

  List<Map<String, Object>> get agruparValoresTransacoes {
    return List.generate(7, (index) {
      final diaSemana = DateTime.now().subtract(Duration(days: index));
      double total = 0.0;

      for (var i = 0; i < transacoesRecentes.length; i++) {
        if (transacoesRecentes[i].data.day == diaSemana.day &&
            transacoesRecentes[i].data.month == diaSemana.month &&
            transacoesRecentes[i].data.year == diaSemana.year) {
          total += transacoesRecentes[i].valor;
        }
      }
      return {
        'dia': DateFormat('E', 'pt_BR')
            .format(diaSemana)
            .substring(0, 1)
            .toUpperCase(),
        'valor': total
      };
    }).reversed.toList();
  }

  double get _gastoTotal {
    return agruparValoresTransacoes.fold(0.0, (previousValue, element) {
      return previousValue += element['valor'];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 6,
        margin: EdgeInsets.all(20),
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: agruparValoresTransacoes.map((dado) {
              return Flexible(
                fit: FlexFit.tight,
                  child: BarraGrafico(
                      dado['dia'],
                      dado['valor'],
                      _gastoTotal == 0.0
                          ? 0.0
                          : (dado['valor'] as double) / _gastoTotal)
                );
            }).toList(),
          ),
        ),
    );
  }
}

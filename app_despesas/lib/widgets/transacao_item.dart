import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:app_despesas/models/transacao.dart';



class ItemTransacao extends StatelessWidget {
  const ItemTransacao({
    Key key,
    @required this.transacao,
    @required this.removerTransacao,
  }) : super(key: key);

  final Transacao transacao;
  final Function removerTransacao;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: Container(
          margin: const EdgeInsets.symmetric(vertical: 6, horizontal: 15),
          decoration: BoxDecoration(
              border: Border.all(
                  color: Theme.of(context).primaryColor, width: 2)),
          padding: const EdgeInsets.all(10),
          child: Text(
            'R\$${transacao.valor.toStringAsFixed(2)}',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                color: Theme.of(context).primaryColor),
          ),
        ),
        title: Text(
          transacao.titulo,
          style: Theme.of(context).textTheme.headline6,
        ),
        subtitle: Text(
          DateFormat('dd/MM/yyyy').format(transacao.data),
          style: TextStyle(color: Colors.grey),
        ),
        trailing: Container(
          child: IconButton(
            icon: const Icon(Icons.delete),
            color: Theme.of(context).errorColor,
            onPressed: () => removerTransacao(transacao.id),
          ),
        ),
      ),
    );
  }
}
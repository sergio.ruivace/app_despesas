import 'package:app_despesas/widgets/transacao_item.dart';
import 'package:flutter/material.dart';

import '../models/transacao.dart';

class TransacaoList extends StatelessWidget {
  final List<Transacao> transacoes;
  final Function removerTransacao;

  TransacaoList(this.transacoes, this.removerTransacao);

  @override
  Widget build(BuildContext context) {
    return transacoes.isEmpty
        ? LayoutBuilder(
            builder: (ctx, constraints) {
              return Column(
                children: <Widget>[
                  Text(
                    'Sem transações',
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                      height: constraints.maxHeight * 0.6,
                      child: Image.asset('assets/images/waiting.png',
                          fit: BoxFit.cover))
                ],
              );
            },
          )
        : ListView(
            children: transacoes
                .map((tx) => ItemTransacao(
                    key: ValueKey(tx.id), transacao: tx, removerTransacao: removerTransacao))
                .toList(),
          );
  }
}

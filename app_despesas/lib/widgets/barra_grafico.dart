import 'package:flutter/material.dart';

class BarraGrafico extends StatelessWidget {
  final String texto;
  final double valorGasto;
  final double porcentagemGasta;

  const BarraGrafico(this.texto, this.valorGasto, this.porcentagemGasta);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (ctx, constrants) {
        return Column(
          children: <Widget>[
            Container(
              height: constrants.maxHeight * 0.15,
              child:
                  FittedBox(child: Text('R\$${valorGasto.toStringAsFixed(0)}')),
            ),
            SizedBox(height: constrants.maxHeight * 0.05),
            Container(
              height: constrants.maxHeight * 0.6,
              width: 10,
              child: Stack(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey, width: 1.0),
                        color: Color.fromRGBO(220, 220, 220, 1),
                        borderRadius: BorderRadius.circular(10)),
                  ),
                  FractionallySizedBox(
                    heightFactor: porcentagemGasta,
                    child: Container(
                        decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            borderRadius: BorderRadius.circular(10))),
                  )
                ],
              ),
            ),
            SizedBox(height: constrants.maxHeight * 0.05),
            Container(
              height: constrants.maxHeight * 0.15,
              child: FittedBox(child: Text(texto)),
            )
          ],
        );
      },
    );
  }
}

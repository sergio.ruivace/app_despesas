import 'package:flutter/foundation.dart';

class Transacao {
  @required final String id;
  @required final String titulo;
  @required final double valor;
  @required final DateTime data;

  Transacao({this.id, this.titulo, this.valor, this.data});
}